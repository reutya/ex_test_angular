// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAYjaO43P7QWNXD3fNK_4O4-cOtjv8gSQg",
  authDomain: "extest-29cae.firebaseapp.com",
  databaseURL: "https://extest-29cae.firebaseio.com",
  projectId: "extest-29cae",
  storageBucket: "extest-29cae.appspot.com",
  messagingSenderId: "550455634578",
  appId: "1:550455634578:web:77f66081bf75fc045dd44b",
  measurementId: "G-3WHPKZ94PR"
  }  
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
