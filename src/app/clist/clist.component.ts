import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clist',
  templateUrl: './clist.component.html',
  styleUrls: ['./clist.component.css']
})
export class ClistComponent implements OnInit {
  panelOpenState = false;

  books:any;
  userId:string;
  books$:Observable<any>;
  
  constructor(private booksservice:BooksService,
    public authService:AuthService) { }
 
    deleteBook(id:string){
    this.booksservice.deleteBook(id,this.userId);
  }

  

  ngOnInit() { 
    this.authService.user.subscribe(
    user=>{
      this.userId=user.uid;
      this.books$ = this.booksservice.getBooks(this.userId);
  }
)
    
  }

}
