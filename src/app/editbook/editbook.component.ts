import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-editbook',
  templateUrl: './editbook.component.html',
  styleUrls: ['./editbook.component.css']
})
export class EditbookComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string; 
  cn:any;
  cas:object[] = [{id:0, name:'business'},{id:1, name:'entertainment'},{id:2, name:'politics'},{id:3, name:'sport'},{id:4, name:'tech'}];
  
  idd: number;
  doc:string;
  id:string;
  userId:string;
  
  onSubmit1(){
  
    console.log(this.userId);
    console.log(this.doc);
    console.log(this.category);
    this.booksservice.updateBook(this.userId,this.id,this.doc,this.category);
    this.router.navigate(['/list']);
  }
  onSubmit(){
    if (this.cn === 'business') {
      this.idd=0;
     // alert(this.id);
    }else if (this.cn === 'entertainment') {
      this.idd=1;
     // alert(this.id);
    }else if (this.cn === 'politics') {
      this.idd=2;
     // alert(this.id);
    }else if (this.cn === 'sport') {
      this.idd=3;
     // alert(this.id);
    }else if (this.cn === 'tech') {
      this.idd=4;
     // alert(this.id);
    }
    console.log(this.id);
     this.category = this.classifyService.categories[this.idd];
     this.categoryImage = this.imageService.images[this.idd];
  }

  constructor(public classifyService:ClassifyService,
              public imageService:ImageService,private booksservice:BooksService, 
              private router:Router ,private route:ActivatedRoute,
              public authService :AuthService) {}



  ngOnInit() {
    
    this.id=this.route.snapshot.params.id;
    this.authService.user.subscribe(
      user=>{
        this.userId = user.uid;
       
         
       
          this.booksservice.getBook(this.id,this.userId).subscribe(
            book=>{
              this.doc=book.data().doc;
              this.category=book.data().category;
              if (this.category === 'business') {
                this.idd=0;
              }else if (this.category === 'entertainment') {
                this.idd=1;
               // alert(this.id);
              }else if (this.category === 'politics') {
                this.idd=2;
               // alert(this.id);
              }else if (this.category === 'sport') {
                this.idd=3;
               // alert(this.id);
              }else if (this.category === 'tech') {
                this.idd=4;
               // alert(this.id);
              }
              this.categoryImage = this.imageService.images[this.idd];
              
            
            }

          ) 
        
          console.log(this.id);
        }

        )
      
    
      }
    
    
}
