import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User|null>
 


  constructor(public afAuth:AngularFireAuth,private router:Router) { 
    this.user = this.afAuth.authState;
  }


  Signup(email:string, password:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .then(res => 
          {
            console.log('Succesful sign up!!!!!!',res);
            this.router.navigate(['/suc']);
            
          }
        );
  }



  // ------------------------------
  // Signup(email:string, password:string)
  // {
  //   console.log('In SignUp()')
  //   console.log("this is the email: ",email)
  //   console.log("this is the password: ",password);
  //   this.afAuth.auth.createUserWithEmailAndPassword(email,password).then(
  //                                                                         user =>
  //                                                                         {
  //                                                                           console.log('Succesful sign up')
  //                                                                           this.router.navigate(['/suc']);
  //                                                                         }
  //                                                                       )
  //                                                                       .catch(function(error) 
  //                                                                                       {
  //                                                                                         // Handle Errors here.
  //                                                                                         var errorCode = error.code;
  //                                                                                         var errorMessage = error.message;
  //                                                                                         if (errorCode === 'auth/wrong-password') {
  //                                                                                           alert('Wrong password.');
  //                                                                                         }
  //                                                                                         else {
  //                                                                                           alert(errorMessage);
  //                                                                                         }
  //                                                                                         console.log(error);
  //                                                                                       });                                                                  

  // }


  // ----------------------------

  Logout(){
    this.afAuth.auth.signOut().then(res=>console.log('succses logout'))
  }

  
  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
          // res => console.log('Succesful Login',res)
         // res => console.log(this.user.subscribe(user=>console.log(user.uid)))
         user=>{this.router.navigate(['/suc'])}
        )
  }


}

