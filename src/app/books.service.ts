import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { MatGridTileHeaderCssMatStyler } from '@angular/material';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
  
})

export class BooksService {
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  bookCollection:AngularFirestoreCollection ;


getBooks(userId:string):Observable<any[]>{
  this.bookCollection=this.db.collection(`users/${userId}/books`);
  return this.bookCollection.snapshotChanges().pipe(
    map(
      collection=>collection.map(
        document=>{
          const data= document.payload.doc.data();
          data.id= document.payload.doc.id;
          console.log(data);
          return data;
        }
      )
    )
  )
}
getBook(id:string,userId:string):Observable<any>{
  
  return this.db.doc(`users/${userId}/books/${id}`).get();
 }
 
 addBook(userId:string,doc_:string,category_:string){
  const book= {doc:doc_,category:category_};
  this.userCollection.doc(userId).collection('books').add(book);
 }

deleteBook(id:string,userId:string){
  this.db.doc(`users/${userId}/books/${id}`).delete();
}

updateBook(userId:string,id:string,doc_:string,category_:string){
  this.db.doc(`users/${userId}/books/${id}`).update({doc:doc_,category:category_})

}


constructor(private db:AngularFirestore) { }
}
